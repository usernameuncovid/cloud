console.log("Hello world form another file 2!!!")



/*
Data types:
undefined, null, number, string, boolean, objects
*/

// Scope = all program
var myName = "Uncovid";
console.log(myName);
// Scope =  in the block
let myCity = "Bogotá";
console.log(myCity);

//Constantes
const PI = 3.1416;
console.log(PI);

var a,b;
a=3;   // assign a constant
b=a;   // assign a variable
c=a+b; // assign a expression
console.log(a,b,c);

var sum= 10 + 10;  // add
sum++;             // increment
sum +=4;           // increment operator
console.log(sum);

var subs = 10 - 10;
subs--;
subs -= 3;
console. log(subs);

var mult = 2 * 3;
mult *= 2;
console.log("multiplicación =",mult);

var divide = 10.5 / 3.7;
divide /= 2;
console.log(divide);

var remainder = 20 % 6;
console.log(remainder);


/* CADENA DE CARACTERES */


var firstName = "PRimero"
var lastName = "ASDASD"

console.log(firstName, lastName);
/*
\'
\"
\n
\t
*/


var phrase1 = 'este es el "carácter"'
console.log(phrase1);

var phrase2= `carácter=' y carácter"`
console.log(phrase2);

var themovie = "Star" + "Wars";
console.log(themovie);

// Concatenate different types

var year = 1983;
themovie += " - " + year;
console.log(themovie);
// Length of string
var length = themovie.length;
console.log(length);
// Position in string (zero based)
var letter = themovie[0];
console.log(letter);
themovie [0] = "H" // it doesn't raise en error
console.log(themovie);



/*
  ARREGLOS
*/

console.log("arreglos");


// Arrays  (Multiple Types)
var elements = ["Cata", 18, "Hi Motherfucker"];
console.log(elements);

// Access an element by index
var element = elements[0];
console.log(element);
elements[1] = 20;
console.log(elements);
// Appende data at the end of array
elements.push(200);
console.log(elements);
//Remove last element: pop
elements.pop();
console.log(elements);
//Remove last element: shift
elements.shift();
console.log(elements);
//add element at start : unshift
elements.unshift("Medellin");
console.log(elements);
//Copy of an array
var elements2=[...elements];
elements2.push("Colombia");
console.log(elements2);
console.log(elements);

var matrix = [
    ["A", "B", "C"],
    ["D", "E", "F"],
    ["G", "H", "I"],
];

// console.log(matrix);

console.log(matrix[0][1]);
matrix.push(["J", "K", "L"]);
console.log(matrix);

// FUNCIONES


// Define function
function myFunction(){
    console.log("This is my Function!");
}

// Call the function
myFunction();

// Function with arguments
function suma (num1, num2){
    return num1 + num2;
}
// Call function with arguments
var result = suma(4, 3);
console.log(result);

// OPTIOONAL PARAMETERS
function incrementValue(value, increment = 1){
    return value + increment;
}

console.log(incrementValue(5, 2 ));
console.log(incrementValue(10));


//  VAR, LET, CONST
var globalVariable = 7;
function function1() {
    let localVariable = 5;
    console.log(globalVariable);
    if(typeof localVariable != undefined)
    {

        console.log("Existe");
    }
    else{
         console.log("No Existe");
    }
}

function1();

// VAKLOR-REFERENCIA.

function agregar(lista, elemento) {
    lista.push(elemento);
    elemento += 1;   
}

let elementos = [1, 2 , 3 , 4 , 5];
let numerito= 10;

console.log(elementos);
console.log(numerito);
// arreglos referencia



// ARROW FUNCTIONS

function mensaje_clasico() {
    console.log('hola');
}

const mensaje_arrow = () =>console.log('arrow');

mensaje_clasico();
mensaje_arrow();

const sumita = (numero1,numero2) =>{
    let resultado;
    resultado = numero1 + numero2;
    return resultado;
}


let x = 5;

if (x==5) {
    console.log('la variable tiene valor 5');
}


if (x % 2 == 0) {
    console.log('el numero es par');
} else {
    console.log("el numero es impar");
}


// multiples...

let valorcito = 0;

if (valorcito == 0) {
    console.log("es cero");
} else if (valor<0){
    console.log(" es positivo");
}else{
    console.log("es negativo");
}

// SWITCH

let dia = 1;

switch (dia) {
    case 1:
        console.log('lunitunes');
        break;

    case 2:
        console.log('marte ');
        break;

    case 3:
        console.log('miercoles');
        break;

    case 4:
        console.log('juerners');
        break;

    case 5:
        console.log('vierne');
        break;

    

    default: 
        console.log('el cuerpo lo sabe');
        break;
}


let numerote = 3;
let cadenadetexto ="3";

if (numerote === cadenadetexto) {
    console.log('son re igualitas');    
} else {
    console.log('son feas y distintas');
}

// TERNARIA

let edad = 18;


/* let mensajejote = edad >> 18 ? "adulto" : "niño";
console.log(mensajetote);
 */
//CICLOS 

var xx= 0;
while (xx<5 ) {
    console.log(xx);
    xx++;
}

for (let xx = 0; xx < 5; xx++) {
    console.log(xx);
    
}

let arreglito = [ 10, 20 ,30, 40]
 for (let x = 0; x < arreglito.length; x++) {
     console.log(arreglito[x]);
     
 }

 let arreme = [1, 2, 3, 4];
 let ress = arreme.join("~");
 console.log(ress);

 ress = arreme.slice(1,3);
 console.log(ress);

 ress = arreme.reduce((x,y) => x+y);
 console.log(ress);

  ress = arreme.map (e => e*2);
 console.log(ress);

 ress = arreme.sort();
 console.log(ress);

 arreme.forEach(xx=> console.log(xx));

 arreme.forEach(x => {
     console.log(x);
     console.log(x+1);
 });
 

 let nu = [2,8,4,1,5,3];

 ress= nu.every((x)=> x %    2 == 0);

 console.log(ress);

 ress = nu.some((x)=> x % 2 == 0);
 console.log(ress);

ress = nu.filter((x)=> x % 2 == 0);
console.log(ress);



//OBJETOS


let usuario = {
    nombre: "Mi vecina",
    eda: 17,
    altura: 1.66,
    medidas: [93, 60, 96],
    país: "Colombia, Bogotá"
,
canciones: [
    {nombre: "reggueton todo el dia :c"}
], 
 susSecretos(){
     return this.medidas.length;
 }
};
console.log(usuario);

console.log(usuario.nombre);
console.log(usuario["nombre"]);
usuario.nombre = "Andrea";
console.log(usuario);


// var, let, const


const ciudad = [
    nombre1= "bogotá",
    nombre2 = "Colombia"
]

Object.freeze(ciudad);
// no modifica


// CLASES


class Persona  {
    constructor(nombre, edad){
        this._nombre = nombre;
        this._edad = edad;
    }
    set nombre(nombre){
        this._nombre = nombre;
    }
    get nombre(){
        return this._nombre;
    }

}

// Creación del objeto
let p = new Persona ("Andrea", 17);
p.nombre = "Paula";
console.log(p.nombre);


//IMPORTAR y EXPORTAR


